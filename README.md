# pdfwordcounter

pdfwordcounter is a commandline application that counts the occurance of words with more than four letters in a pdfs, takeing a folder of pdf files.

## Compile/Install

### Requirements

* git
* c++20 capable compiler (GCC, CLANG)
* [cmake](https://cmake.org/) 3.20 or later
* [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/) (or change/hardcode the paths in CMakeLists.txt)
* [libpoppler](https://poppler.freedesktop.org)

### Procedure (UNIX)

In a console do:

* git clone https://git-ce.rwth-aachen.de/carl_philipp.klemm/pdfwordcounter.git
* cd pdfwordcounter
* mkdir build
* cd build
* cmake ..
* make -j8
* sudo make install

## Usage of pdfwordcounter

### Generate spectra

pdfwordcounter [FOLDER]

with FOLDER being a folder containing *.pdf files

## Licence

pdfwordcounter is Licenced to you under the GPLv3. See gpl-3.0.txt for more information
