/**
* pdfwordcounter
* Copyright (C) 2022 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <filesystem>
#include <memory>
#include <poppler-document.h>
#include <poppler-page.h>
#include <poppler-global.h>
#include <sstream>
#include <cassert>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <map>

#include "log.h"
#include "tokenize.h"


static void printUsage(int argc, char** argv)
{
	Log(Log::WARN)<<"An application that, given a directory of pdf files, will tell you how often words are used in those files";
	Log(Log::WARN)<<"Usage: "<<argv[0]<<" [FOLDER]";
}

static void dropMessage(const std::string& message, void* userdata)
{
	(void)message;
	(void)userdata;
}

static std::string getText(poppler::document* document)
{
	std::string text;
	size_t pageCount = document->pages();
	for(size_t i = 0; i < pageCount; ++i)
	{
		text.append(document->create_page(i)->text().to_latin1());
		text.append("\n");
	}
	return text;
}

class CompString
{
public:
	bool operator()(const std::string& a, const std::string& b) const
	{
		for(size_t i = 0; i < a.size(); ++i)
		{
			if(i > b.size())
				return false;
			if(a[i] < b[i])
				return true;
			else if(a[i] > b[i])
				return false;
		}
		return false;
	}
};

static bool isInvalid(const char in)
{
	if(in < 32 || in > 126)
		return true;
	else
		return false;
}

void saveWordMap(const std::map<std::string, size_t, CompString>& wordMap, std::iostream& file)
{
	for(const std::pair<std::string, size_t> word : wordMap)
		file<<word.second<<",\t"<<word.first<<'\n';
}

int main(int argc, char** argv)
{
	Log::level = Log::INFO;
	poppler::set_debug_error_function(dropMessage, nullptr);

	if(argc < 2)
	{
		printUsage(argc, argv);
		return 0;
	}
	else if(argc > 2)
	{
		Log(Log::WARN)<<"This application will process only one directory at a time. Will process: "<<argv[1];
	}

	std::map<std::string, size_t, CompString> wordMap;

	std::fstream file;
	file.open("./map.txt", std::ios_base::out);
	if(!file.is_open())
	{
		Log(Log::ERROR)<<"could not open ./map.txt for writeing";
		return 2;
	}

	std::filesystem::path dirPath(argv[1]);
	if(std::filesystem::is_directory(dirPath))
	{
		for(const std::filesystem::directory_entry& entry : std::filesystem::directory_iterator(dirPath))
		{
			if((entry.is_regular_file() || entry.is_symlink()) && entry.path().extension() == std::string(".pdf"))
			{
				Log(Log::INFO)<<"Processing "<<entry.path()<<" current map size: "<<wordMap.size();
				poppler::document* document = poppler::document::load_from_file(entry.path());
				if(!document)
				{
					Log(Log::WARN)<<"Could not load "<<entry.path();
					continue;
				}

				std::string text = getText(document);

				std::vector<std::string> tokens = tokenize(text, " ,.?!(){}[]\n\t\"'~-|+=@");

				std::map<std::string, size_t, CompString> documentWordMap;
				for(std::string& token : tokens)
				{
					if(token.size() <= 5)
						continue;
					token.resize(std::remove_if(token.begin(), token.end(), &isInvalid) - token.begin());
					size_t numericCount = 0;
					for(char& ch : token)
					{
						if(ch < 65)
							++numericCount;

						if(ch >= 65 && ch <= 90)
							ch = ch + 32;
					}

					if(token.size() <= 5 || numericCount/static_cast<double>(token.size()) > 0.5)
						continue;
					auto iterator = documentWordMap.find(token);
					if(iterator == documentWordMap.end())
						documentWordMap.insert({token, 1});
					else
						++documentWordMap.at(token);
				}

				for(const std::pair<std::string, size_t> word : documentWordMap)
				{
					size_t wordCount = word.second < 5 ? word.second : 5;
					auto iterator = wordMap.find(word.first);
					if(iterator == wordMap.end())
						wordMap.insert({word.first, wordCount});
					else
						wordMap.at(word.first) += wordCount;
				}

				delete document;
			}
		}
	}
	else
	{
		Log(Log::ERROR)<<dirPath<<" must be a valid directory";
		return 1;
	}

	saveWordMap(wordMap, file);
	file.close();

	return 0;
}
